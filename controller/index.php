<?php
namespace Controller;
use \bin\Controller as Controller;
class Index extends Controller{
	function __construct() {
		parent::__construct();
	}

	public function index() {
			$this->view->render('index/index.php');
	}
	public function all(){

		$allowedEntities = array('news','program','about','enquete','study','practice','sponsors','contact');
		$entities = empty($_POST['entities']) ? $allowedEntities :$_POST['entities'];
		$result = new \stdClass();
		foreach($entities as $entity){
			$name = (is_string($entity)) ? $entity : $entity['name'];

			$fields = (!is_string($entity) && isset($entity['fields'])) ? $entity['fields']: NULL;
			$result->{$name} = $this->model->selectAllQuery($name,$fields);
			foreach($result->{$name}['data'] as $key => $value){
				if(isset($value['createdOn']))
					$result->{$name}['data'][$key]['createdOn'] = date('d-m-Y',strtotime($value['createdOn']));
				if(isset($value['body']))
					$result->{$name}['data'][$key]['body'] = $this->model->getPngFromLatex($value['body']);
				if(isset($value['question']))
					$result->{$name}['data'][$key]['question'] = $this->model->getPngFromLatex($value['question']);
				if(isset($value['answer']))
					$result->{$name}['data'][$key]['answer'] = $this->model->getPngFromLatex($value['answer']);
			}
		}
		$this->view->renderJSONP($result);
	}
}