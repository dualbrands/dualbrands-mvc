<?php
/**
 * Created by PhpStorm.
 * User: Wouter van Ooijen
 * php developer at DualBrands
 * Mail: w.vanooijen@dualbrands.nl
 * Date: 23-2-14
 * Time: 22:15
 */
namespace Controller;
use \bin\Controller as Controller;
class User extends Controller{
    function __construct() {
        parent::__construct();
        //$this->loginRequired();
    }
    public function index() {
        $this->admin();
    }
    public function add() {

        if($this->isAjaxCall() && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['pass']) &&
            !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['pass'])
        ){
            $r = array();
            $r['success'] = $this->model->add($_POST);
            //set loginsession
            if(!empty($r['success']) && $r['success'] > 0){
                $_SESSION['uid'] = $r['success'];
                $r['user'] = $this->model->getUserById($r['success']);
            }
            $this->view->renderJSONP($r);
        }
    }
    public function update() {
        $this->user->isLoggedIn();

        if($this->isAjaxCall() && isset($_POST['id']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['role']) &&
            !empty($_POST['id']) && !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['role'])
        ){
            $r = array();
            $r['success'] = $this->model->update($_POST['id'],$_POST['email'],false,$_POST['role']);
            $this->view->renderJSONP($r);
        }
    }
    public function delete(){
        $this->user->isLoggedIn();

        if($this->isAjaxCall() && isset($_POST['id']) && !empty($_POST['id'])){
            $r = array();
            $r['success'] = $this->model->delete($_POST['Uid']);
            $this->view->renderJSONP($r);
        }
    }
    public function login(){
    	$r = array();
        if($this->isAjaxCall() && isset($_POST['name']) && !empty($_POST['name']) && isset($_POST['pass']) && !empty($_POST['pass'])){
            
            $r['status'] = $this->model->login($_POST['name'],$_POST['pass']);
            if($r['status'] == true)
            	$r['user'] = $this->model->getUserById($_SESSION['uid']);
            
        }else{
	        $r['status'] = false;
        }
        $this->view->renderJSONP($r);
        //echo "hallo";
    }
    public function logOut(){
        if($this->isAjaxCall()){
            $r = array();
            $r['status'] = 1;
            $r['result'] = $this->model->logout();
            $this->view->renderJSONP($r);
        }    
    }
    public function current(){
        if($this->isAjaxCall()){
            $r = array();
            $r['status'] = 1;
            $r['result'] = $this->model->getCurrent() ;
            $this->view->renderJSONP($r);
        }    
    }
    public function getAll(){
        if($this->isAjaxCall()){
            $r = $this->model->selectAllQuery('users',(isset($_POST['fields']) ? $_POST['fields']: NULL));
        }else{
            $r = array('status' => 0, 'mssg' => 'No data send');
        }
        $this->view->renderJSONP($r);
    }
}