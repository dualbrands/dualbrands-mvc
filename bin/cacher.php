<?php 
	class Cacher {
		function __construct($output) {
			$path = $this->get_path();
			$last_space = strrpos($path, '/');
			$dir = substr($path, 0, $last_space);
			if(!is_dir($dir)){
				mkdir($dir, 0755);
			}
		    $f = fopen ( $path.'.html', 'c' );
		    fwrite ( $f, $output );
		    fclose ( $f );
		}

		public function cache($output) {
			$path = $this->get_path();
			$last_space = strrpos($path, '/');
			$dir = substr($path, 0, $last_space);
			if(!is_dir($dir)){
				mkdir($dir, 0755);
			}
		    $f = fopen ( $path.'.html', 'c' );
		    fwrite ( $f, $output );
		    fclose ( $f );
		}

		private function get_path(){
			//DOCUMENT_ROOT}/cache/%{ENV:boostpath}/%{ENV:uid}/%{HTTP_HOST}%{REQUEST_URI}_%{QUERY_STRING}
			$uri = explode('?', $_SERVER['REQUEST_URI'], 2);
			return 	'cache/'. $_SERVER['boostpath'] .'/'  . $_SERVER['HTTP_HOST'] 
					.((isset($_SERVER['uid']) && is_numeric($_SERVER['uid']))?'/'.$_SERVER['uid'] :'')
					.$_SERVER['SCRIPT_NAME'].'_'. (isset($uri[1])?$uri[1]:'');
		}
	}