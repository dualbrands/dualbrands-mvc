<?php
namespace bin;
class View {
	public $tpl, $menu, $breadcrumb, $title, $sidebar, $footer, $default_title = 'Default title';
    private $main;

	function __construct() {
        $this->tpl = 'default';
    }
	
    public function render($main) {
        ob_start();
		$title= null;
		if(empty($this->breadcrumb))
	        $this->breadcrumb = $this->create_breadcrumb();
        $this->main = $main;

        ob_flush();

        require_once('views/'.$this->tpl.'/page.php');

        ob_end_flush();
	}

    public function loadTPL($tpl) {
		if (file_exists(getcwd().'/views/'.$this->tpl.'/'.$tpl)){
            //echo $this->tpl.'/'.$tpl;
            ob_flush();
            require_once(getcwd().'/views/'.$this->tpl.'/'.$tpl);
            ob_flush();
        }
    }
    public function loadTplAsStr($tpl) {
        if (file_exists('views/'.$this->tpl.'/'.$tpl)){
            ob_start();
            //echo $this->tpl.'/'.$tpl;
            //ob_flush();
            require_once('views/'.$this->tpl.'/'.$tpl);
            $r = ob_get_contents();
            ob_end_clean();
            return $r;
        }else{
            return 'views/'.$this->tpl.'/'.$tpl;
        }
    }

    public function renderJSON($object) {
        header('Content-type: application/json');
        echo json_encode($object);
    }

    public function renderJSONP($object) {
        header('Content-type: application/json');
        if (isset($_GET["callback"])) {
            header('Content-Type: application/javascript');
            echo $_GET["callback"]."(".json_encode($object, JSON_NUMERIC_CHECK).")";
        } else {
            $this->renderJSON($object);
        }
    }
    public function renderCSV($data,$titles = FALSE){
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');
		if($titles == FALSE){
			$titles = array_keys(reset($data));
		}
		$title[0] = $titles;
		$csv_lines = array_merge($title,$data);
		$output = fopen('php://output', 'w');
		foreach($csv_lines as $line)
			fputcsv($output, $line);
		fclose($output);
	}
	 /**
     * Breadcrumb array needs to have the following structure
     * Array(
     *   [0] =>
     *	    Array(
     * 		    ['title'] = Title in breadcrumb
     * 		    ['href'] = Link for the breadcrumb item, just put the page name in here so like 'book','wiki','buy','course','$coursename'
     *			['alt'] = Google alt link
     *	)

     * @param $brcAr
     * @return string
     */

    public function custom_breadcrumb($brcAr) {
        $data = array();
        $item = $href = $top_url = null;
        foreach ($brcAr as $key=>$brcItem) {
            $data[$key] = null;	//Set the offset.
            //Check if it's the first or last item
            ($key == 0 ? $top_url = NULL : (end($brcAr) == $brcItem ? $item = 'last' : ''));
            if (!empty($brcItem['href'])) {
                $href = $top_url."/". $brcItem['href']; //Set navigation
            }
            $data[$key] .= ($item == 'last' ? "<li class='active'>" : "<li>"); //Check if li has to be active.
            $data[$key] .= (!empty($brcItem['href']) ? "<a href='".$href."'>".$brcItem['title']."</a>" : $brcItem['title']); //Generate HREF
            $data[$key] .= ($item != 'last' ? "<span class='divider'>/</span>" : "") . "</li>"; //Finish li
            //$top_url = $href;	//Set new top url
        }
        return "<ul class='breadcrumb'>".implode($data)."</ul>";
    }

    /**
     * Creates the breadcrumb
     * @param null $title
     * @return bool|string
     */
    private function create_breadcrumb($title = null) {
        if (!isset($_GET['url'])) return false;

        $pages = explode("/",$_GET['url']);
        foreach ($pages as $key => $page) {
            if ($key == 0) {
                $top_url = null;
            }
            $bc[$key]['url'] = $top_url."/".$page;
            $pagename = ucfirst(str_replace('_',' ',$page));
            $pagename = ucfirst(str_replace('-',' ',$pagename));
            $data[$key] = (end($pages) != $page) ? "<li><a href='".$bc[$key]['url']."'>".$pagename."</a><span class='divider'>/</span></li>":"<li class='active'>".$pagename."</li>";
            $top_url = $bc[$key]['url'];
        }
        return "<ul class='breadcrumb'>".implode($data)."</ul>";
    }


}

?>