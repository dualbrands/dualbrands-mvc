<?php
namespace bin;
use \bin\View as View;
use \bin\viewModel as viewModel;
	class Controller {
        public $user, $model, $view,$name;
		function __construct($name = NULL) {
			if (isset($_GET['url'])) {
				$url = explode("/", rtrim($_GET['url'], '/'));
				foreach($url as $key => $value){
					$url[$key] = str_replace("-", NULL, $url[$key]);
				}
			} else {
				$url = NULL;
			}
			$this->name = isset($name) && empty($this->name) ? $name: NULL;
			$this->view = new View();
			$this->model = $this->loadModel($url[0]);
            $this->user = $this->loadModel('user');
            
		}

        public function loadModel($name) {
            // Het laden van het goede model. //
            $modelname = "\\model\\".$name."_model";
            if(!empty($name) && $this->classExists($modelname) ){
                $m = new $modelname;
            }else{
                $m = new Model();
            }
            return $m;
        }
        public function loadViewModel($name) {
            // Het laden van het goede model. //
            $modelname = "\\viewmodel\\".$name."_vm";
            //print_r($modelname.$this->classExists($modelname));
            if(!empty($name) && $this->classExists($modelname) && $this->view != NULL){
                $m = new $modelname;
            }else{
                $m = new viewModel();
            }
            //print_r($m);
            return $m;
        }
        private function classExists($className){
            $classFile = str_replace( '\\', DIRECTORY_SEPARATOR, $className );
            $classPI = pathinfo( $classFile );
            $classPath = strtolower( $classPI[ 'dirname' ] );
            if(file_exists( getcwd() . $classPath . DIRECTORY_SEPARATOR . $classPI[ 'filename' ] . '.php' ))
                return TRUE;//return class_exists($className) ? TRUE: FALSE;
        }
        

        // RE EVALUATE!!!
		public function isAjaxCall() {
            return TRUE;
			/* AJAX check  */
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		
		
    /*------------------------*\
        $CALL FUNCTIONS
    \*------------------------*/
    
    public function add() {
        $this->user->isLoggedIn();
    	if($this->isAjaxCall() && !empty($_POST)){
    		$r = $this->model->insertQuery($this->name,$_POST);
    	}else{
    		$r = array('status' => 0, 'mssg' => 'No data send');
    	}
    	$this->view->renderJSONP($r);
    }
    public function updateById() {
        $this->user->isLoggedIn();
    	if($this->isAjaxCall() && !empty($_POST) ){
    		$r = $this->model->updateQuery($this->name,$_POST);
    	}else{
    		$r = array('status' => 0, 'mssg' => 'No data send');
    	}
    	$this->view->renderJSONP($r);
    }
    
    public function deleteBy(){
    	if($this->isAjaxCall() && !empty($_POST)){
    		$r = $this->model->deleteByQuery($this->name,$_POST);
    	}else{
    		$r = array('status' => 0, 'mssg' => 'No data send');
    	}
    	$this->view->renderJSONP($r);
    }
    public function getBy(){
    	if($this->isAjaxCall() && !empty($_POST) && isset($_POST['values'])){
    		$r = $this->model->selectByQuery($this->name,$_POST['values'],(isset($_POST['fields']) ? $_POST['fields']: NULL));
    	}else{
    		$r = array('status' => 0, 'mssg' => 'No data send');
    	}
    	$this->view->renderJSONP($r);
    }
    public function getAll(){
    	if($this->isAjaxCall()){
    		$r = $this->model->selectAllQuery($this->name,(isset($_POST['fields']) ? $_POST['fields']: NULL));
    	}else{
    		$r = array('status' => 0, 'mssg' => 'No data send');
    	}
    	$this->view->renderJSONP($r);
    }
		
	}
?>