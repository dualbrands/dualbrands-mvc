<?php
namespace bin;
use \PDO as PDO;

/**
 * Class Model
 * @package bin
 */
class Model {

    private $key, $salt;
    public $db, $settings,$allowedFields;

    /**
     *
     */
    function __construct() {
        try {
			$config = Config::get();
			$this->key = $config['keys']['private'];
			$this->salt = $config['keys']['salt'];
        	$dbConfig = $config['databases'][0];
            $this->db = new PDO($dbConfig['type'].':host='.$dbConfig['host'].';dbname='.$dbConfig['name'], $dbConfig['user'], $dbConfig['pass']);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            exit;
        }
	}


    // Hash password; first the password is split in half,
    // if the number is uneven the first half will be the largest.
    // Then the second salt is added in between the two parts and
    // the first salt is added after the last part of the password.
    // This all is then hashed using the sha256 algorithm and
    // the salts are pre-/appended.
    /**
     * @param $pw
     * @param $salt1
     * @param $salt2
     * @return array|string
     */
    public function pwhash($pw, $salt1, $salt2) {
        $pw = str_split($pw, ceil(strlen($pw)/2));
        $pw = $salt2.hash("sha256", $pw[0].$salt2.$pw[1].$salt1).$salt1;
        return $pw;
    }

    // Generate a random string of $num characters.
    /**
     * @param $num
     * @return string
     */
    function rand_str($num) {
        $str = "";
        $chars = '0123456789abcdefghijklmnopqrstuvwxyz';

        // Set $i to 0, increment at the end of every run, run as long as $i < $num
        // and randomly select a character from the list.
        for ($i = 0; $i < $num; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)]; // -1 because 0 is also a position
        }
        return $str;
    }

    /**
     * @param $name
     * @return Model
     */
    public function loadModel($name) {
        // Het laden van het goede model. //
        $path = "/model/".$name."_model.php";
        if (file_exists($path) && $this->view != NULL) {
            require_once($path);
            $modelname = $name . '_model';
            //Ondersteuning voor meerdere model per controller.
            return new $modelname;
            //$this->view->model = new $modelname;
        } else {
            return new Model();
            //$this->view->model = new Model();
        }
    }

    /**
     * @param $email
     * @return bool
     */
    public function validateEmail($email) {
		$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		if (preg_match($pattern, $email) === 1) {
			return $email;
		} else {
			return FALSE;
		}
	}

    /**
     * @param $name
     * @return string
     */
    protected function generateMachineName($name) {
	    return strtolower(preg_replace(array(
	      '/[^a-zA-Z0-9]+/',
	      '/-+/',
	      '/^-+/',
	      '/-+$/',
	    ), array('-', '-', '', ''), $name));
	}

    /**
     * @param $input
     * @return mixed
     */
    public function getPngFromLatex($input){
		
        include_once("latexrender/latex.php");
        
        //q=\lim_{x\to +\infty}\left(1+\frac{1}{x}\right)^x
		$replacement = '[preamb]\newcommand{\mycolor}[1]{\Color{0.0 0.2 1 0.1}{#1}}[/preamb][tex]\displaystyle\mycolor{$1}[/tex]';
		$result = preg_replace('/<span class="math-tex">(.*?)<\/span>/', $replacement, $input);
		//echo $result;
		return latex_content($result);
	}
    /*------------------------*\
        $ENCRYPTION FUNCTIONS
    \*------------------------*/
    /**
     * @param $data
     * @return string
     */
    protected function encrypt($data) {
        $data = $data.$this->salt;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $n = 5;
        while ($n != 0) {
            $data = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->key, $data, MCRYPT_MODE_ECB, $iv);
            $n--;
        }
        return $data;
    }

    /**
     * @param $data
     * @return array|string
     */
    protected function decrypt($data) {
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $n = 5;
        while ($n != 0) {
            $data = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $this->key, $data, MCRYPT_MODE_ECB, $iv);
            $data = explode($this->salt,$data);
            $data = $data[0];
            $n--;
        }
        return $data;
    }
    /*------------------------*\
        $CACHING FUNCTIONS
    \*------------------------*/
    /**
     * @param $k
     * @param $v
     * @param int $t
     * @return array|bool
     */
    function cacheStore($k, $v, $t = 3600) {
        global $caching;
        if ($caching === TRUE) {
            return apc_store($k, $v, $t);
        }
        else {
            return FALSE;
        }
    }

    /**
     * @param $k
     * @return bool|mixed
     */
    function cacheFetch($k) {
        global $caching;
        if ($caching === TRUE) {
            return apc_fetch($k);
        }
        else {
            return FALSE;
        }
    }

    /**
     * @param $k
     * @return bool|\string[]
     */
    function cacheDelete($k) {
        global $caching;
        if ($caching === TRUE) {
            return apc_delete($k);
        }
        else {
            return FALSE;
        }
    }

    /**
     * @return bool
     */
    function cacheClear() {
        return apc_clear_cache('user');
    }
    /*------------------------*\
        $DB FUNCTIONS
    \*------------------------*/
    /**
     * @param $table
     * @param $values
     * @return array
     */
    public function insertQuery($table,$values){
    	//print_r($this->allowedFields); echo "<br/>"; print_r($values);
    	$binds = array();
    	$fields = array();
    	$query = array();
    	$vals = array();
    	foreach($values as $field => $value){
    		if(!isset($this->allowedFields[$field]))
    			return array('status'=> 0, 'mssg' => 'Wrong input field: '.$field);
    		$binds[] = $this->getDataType($this->allowedFields[$field]);
    		$query[] = '?';
    		$fields[] = $field;
    		$vals[] = $value; 
    	}
    	$q = 'INSERT INTO '. $table .' ('.implode(',', $fields).') VALUES ('.implode(' , ', $query).')';
    	try{
	    	$sth = $this->db->prepare($q);
	    	foreach($binds as $key => $dataType)
	    		$sth->bindParam($key + 1, $vals[$key],$dataType);
	    	
	    	return $sth->execute() ?  array('status' => 1 , 'data' => array('id' => $this->db->lastInsertId())) : array('status' => 0 , 'data' => FALSE) ;
		} catch (PDOException $e) {
			//print "Error!: " . $e->getMessage() . "<br/>";
	    	return array('status' => 0 , 'data' => NULL,'mssg'=>$e->getMessage());
		}
    }

    /**
     * @param $table
     * @param $values
     * @param null $fields
     * @return array
     */
    public function selectByQuery($table,$values,$fields = NULL){
    	//print_r($this->allowedFields); echo "<br/>"; print_r($values);
    	$query = array();
    	$binds = array();
    	$vals = array();
    	foreach($values as $field => $value){
    		if(!isset($this->allowedFields[$field]))
    			return array('status'=> 0, 'mssg' => 'Wrong input field: '.$field);
    		$binds[] = $this->getDataType($this->allowedFields[$field]);
    		$query[] = $field.' = ?';
    		$vals[] = $value; 
    	}
    	$what = $fields == NULL ? '*' : implode(',', $fields);
    	$q = 'SELECT '.$what.' FROM '. $table .' WHERE '.implode(' AND ', $query);
    	try{
	    	$sth = $this->db->prepare($q);
	    	foreach($binds as $key => $dataType)
	    		$sth->bindParam($key + 1, $vals[$key],$dataType);
	    	$sth->execute();
	    	$sth->setFetchMode(\PDO::FETCH_ASSOC);
	    	return array('status' => 1 , 'data' => $sth->fetchAll());
		} catch (PDOException $e) {
			//print "Error!: " . $e->getMessage() . "<br/>";
	    	return array('status' => 0 , 'data' => NULL,'mssg'=>$e->getMessage());
		}
    }

    /**
     * @param $table
     * @param null $fields
     * @return array
     */
    public function selectAllQuery($table,$fields = NULL){
    	$what = $fields == NULL ? '*' : implode(',', $fields);
    	$q = 'SELECT '.$what.' FROM '. $table ;
    	try{
	    	$sth = $this->db->prepare($q);
	    	$sth->execute();
	    	$sth->setFetchMode(\PDO::FETCH_ASSOC);
	    	return array('status' => 1 , 'data' => $sth->fetchAll());
		} catch (PDOException $e) {
			//print "Error!: " . $e->getMessage() . "<br/>";
	    	return array('status' => 0 , 'data' => NULL,'mssg'=>$e->getMessage());
		}
    }

    /**
     * @param $table
     * @param $values
     * @return array
     */
    public function updateQuery($table,$values){
    	//print_r($this->allowedFields); echo "<br/>"; print_r($values);
    	$query = array();
    	$binds = array();
    	$vals = array();
    	foreach($values as $field => $value){
    		if(!isset($this->allowedFields[$field]))
    			return array('status'=> 0, 'mssg' => 'Wrong input field: '.$field);
    		if($field !== 'id'){
    			$query[] = $field.' = ?';
    			$binds[] = $this->getDataType($this->allowedFields[$field]);
				$vals[] = $value; 
    		}else{
    			$where = array('query' => $field .'= ?', 'value' => $value,'dataType' => $this->getDataType($this->allowedFields[$field]));
    		}
    	}
    	if(empty($where))
    		return array('status' => 0 , 'data' => NULL,'mssg'=>'No id has been sent');
    	$q = 'UPDATE '. $table .' SET '.implode(' , ', $query).' WHERE '. $where['query'];
    	
    	try{
	    	$sth = $this->db->prepare($q);
	    	foreach($binds as $key => $dataType)
	    		$sth->bindParam($key + 1, $vals[$key],$dataType);
	    	$sth->bindParam(count($binds) + 1, $where['value'],$where['dataType']);
	    	$sth->execute();
	    	$sth->setFetchMode(\PDO::FETCH_ASSOC);
	    	return array('status' => 1 , 'data' => $where['value']);
		} catch (PDOException $e) {
			//print "Error!: " . $e->getMessage() . "<br/>";
	    	return array('status' => 0 , 'data' => NULL,'mssg'=>$e->getMessage());
		}
    
    }

    /**
     * @param $table
     * @param $values
     * @return array
     */
    public function deleteByQuery($table,$values){
    	//print_r($this->allowedFields); echo "<br/>"; print_r($values);
    	$query = array();
    	$binds = array();
    	$vals = array();
    	foreach($values as $field => $value){
    		if(!isset($this->allowedFields[$field]))
    			return array('status'=> 0, 'mssg' => 'Wrong input field: '.$field);
    		$binds[] = $this->getDataType($this->allowedFields[$field]);
    		$query[] = $field.' = ?';
    		$vals[] = $value; 
    	}
    	$q = 'DELETE FROM '. $table .' WHERE '.implode(' AND ', $query);
    	try{
	    	$sth = $this->db->prepare($q);
	    	foreach($binds as $key => $dataType)
	    		$sth->bindParam($key + 1, $vals[$key],$dataType);
	    	return array('status' => 1 , 'data' => ( $sth->execute() ? TRUE : FALSE ));
		} catch (PDOException $e) {
			//print "Error!: " . $e->getMessage() . "<br/>";
	    	return array('status' => 0 , 'data' => NULL,'mssg'=>$e->getMessage());
		}
    }

    /**
     * @param $dataType
     * @return int|null
     */
    private function getDataType($dataType){
    		switch($dataType){
    			case 'str':
    			case 'timestamp':
    				$dataType = \PDO::PARAM_STR;
    				break;
    			case 'int':
    				$dataType = \PDO::PARAM_INT;
    				break;
    			case 'blob':
    				$dataType = \PDO::PARAM_LOB;
    				break;
    			case 'float':
    			default:
    				$dataType = NULL;
    				break;
    		}
    	return $dataType;
    }
    
}