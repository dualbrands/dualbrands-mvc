<?php 
namespace bin;
use controller\error as Error;
class Bootstrap {
	public $allowedEntities;
	function __construct() {
		if (isset($_GET['url'])){
			$url = explode("/", rtrim($_GET['url'], '/'));
			foreach($url as $key => $value) {
				$url[$key] = str_replace("-", NULL, $url[$key]);
			}
		} else {
			$url = NULL;
		}
		try{
            if(empty($config))
            	Config::set(getcwd().'/config/config.yml');
			$allowedEntities = Config::get()['allowedEntities'];
			$this->allowedEntities = $allowedEntities;
            
			if (empty($url[0]))
                $url = array('index','index');

            $controllerName = "\\controller\\".$url[0];
            if(class_exists($controllerName)){
                $controller = new $controllerName();
            }else{
            	if(in_array($url[0], $this->allowedEntities)){
            		$controller = new controller($url[0]);
            	}else{
                	throw new \Exception('Class kan niet worden gevonden, neem contact op met de ontwikkelaar.',404);
                }
            }
            // calling methods
            if (isset($url[2])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}($url[2]);
                } else {
                    throw new \Exception('Method met Parameter kan niet worden gevonden, neem contact op met de ontwikkelaar.',404);
                }
            } else {
                if(!isset($url[1]))
                    $url[1] = 'index';
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    throw new \Exception('Method kan niet worden gevonden, neem contact op met de ontwikkelaar. Error File: '.__FILE__.', Line :'.__LINE__,404);
                }
            }

        }catch(\PDOException $e) {
            echo "An error occurred: <br /><br />".$e->getMessage()." in ".$e->getFile()." on line ".$e->getLine();
            return FALSE;
        }
        catch(\Exception $e){
            $this->error($e->getMessage(),$e->getCode());
        }
    }

	function error($message = false,$code = false) {
		require_once('controller/error.php');
		$controller = new Error();
        /*
        if(method_exists($controller,'e'.$code)){
            $controller->{'e'.$code}($message);
        }else{
            $controller->index($message);
        }
        */
        if($code == false) $code = 404;
        if($message == false ) $message = 'Er is iets misgegaan';
        $controller->error($message,$code);
        return false;
	}
}