<?php
namespace bin;
class Session {
	function __construct() {
		@session_start();
	}

	public static function init() {
		@session_start();
	}

	public static function get($key) {
		return $_SESSION[$key];
	}

	public static function set($key, $value) {
		$_SESSION[$key] = $value;
	}

    public static function remove($key) {
        unset($_SESSION[$key]);
    }

	public static function destroy() {
		//unset($_SESSION);
		session_destroy();
	}

	public static function checkLogIn() {
		return (self::get('loggedIn'))?TRUE:FALSE;
	}

	public static function checkAdmin() {
		return (self::get('role'))?TRUE:FALSE;
	}

	public static function getUID() {
		return (self::get('userid'))?self::get('userid'):FALSE;
	}
}
?>