<?php 
namespace bin;
class Config {
	private static $initialized = false;
	private static $config;
	
	private static function initialize($yml){
		if (self::$initialized)
    		return;
    	self::$config = \bin\Spyc::YAMLLoad($yml);
		self::$initialized = true;
	}
	public static function get($yml = 'false'){
		self::initialize($yml);
		return self::$config;
	}
	public static function set($yml = 'false'){
		self::initialize($yml);
		return;
	}
	
	function __constuct(){}
}