<?php
namespace Model;
use \bin\Model as Model;
class sponsors_model extends Model{
	function __construct(){
		parent::__construct();
		$this->allowedFields = array('id'=>'int','title'=>'str','description'=>'blob','url'=>'str');
	}
	
}