<?php
namespace Model;
use \bin\Model as Model;
class user_model extends Model{
	private $user;
    private $userId;
    private $permissions = array();

	function __construct(){
		parent::__construct();
        $this->userId = isset($_SESSION['uid']) && !empty($_SESSION['uid']) ? $_SESSION['uid'] : FALSE;
		if($this->isLoggedIn() != FALSE){
			$this->user = $this->getUserById($_SESSION['uid']);
		}else{
			$this->user = FALSE;
		}
	}
	
	public function getCurrent(){
		if ($this->isLoggedIn()){
            if(isset($this->user) && !empty($this->user)){
               return $this->user;
            }else{
                $this->user = $this->getUserById($this->userId);
                return $this->user;
            }
        }else{
            return FALSE;
        }
	}

	public function getUid(){
		return $this->userId;
    }

	public function isLoggedIn(){
		return (isset($_SESSION['uid'])) ? TRUE : FALSE;
	}
	
    public function login($user,$pass){
        try{
            $sth = $this->db->prepare("SELECT id,pass FROM users WHERE name = :name LIMIT 1");
            
			$sth->bindParam(":name", $user, \PDO::PARAM_STR);
            $sth->execute();
            
			$sth->setFetchMode(\PDO::FETCH_ASSOC);
            $rsh = $sth->fetch();
            
            if (!$rsh) {
                die("An error occurred while retrieving your account information.");
            }

            $salt1 = substr($rsh["pass"], -8);
            $salt2 = substr($rsh["pass"], 0, 8);
            // If the hashed password is equal to the stored hash. //
            if ($this->pwhash($pass, $salt1, $salt2) === $rsh["pass"]) {
                $_SESSION['uid'] = $rsh['id'];
                return TRUE;
            } else {
                return FALSE;
            }
        }catch(PDOException $e){
            echo "oeps";
        }
    }
    public function logout() {
		if(isset($_SESSION['uid'])){
		  unset($_SESSION['uid']);
		  unset($this->user);
		  return TRUE;
		}else{
			return FALSE;
		}
    }
    public function getUserById($id){
        $sth = $this->db->prepare("SELECT id,name,email FROM users  WHERE id = :id LIMIT 1");
        $sth->execute(array(':id' => $id));
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        $data = $sth->fetch();
        return $data;
    }

    public function getAll(){
        $sth = $this->db->prepare("SELECT id,name,email FROM users");
        $sth->setFetchMode(\PDO::FETCH_ASSOC);
        $sth->execute();
        $data = $sth->fetchAll();
        return $data;
    }


    public function add($user){
        $sth = $this->db->prepare("INSERT INTO users (name,email,pass) VALUES (:name,:email,:pass)");
        $salt1 = $this->rand_str(8);
        $salt2 = $this->rand_str(8);

        return $sth->execute(array(
            ':name' => $user['name'],
            ':email' => $user['email'],
            ':pass' => $this->pwhash($user['pass'], $salt1, $salt2)
        )) ? $this->db->lastInsertId('id'):FALSE;
    }

    //LOGIN METHODS

	public function createAccount($email,$password,$organization,$role){
		if($email!='' && $password != ''){
			$password = $this->encrypt($password);
			$activateUrl = urlencode($this->encrypt(date("Y-m-d H:i:s")));
			if(!$this->email_exists($email) == FALSE){
				return FALSE;
			}
			$sth = $this->db->prepare('INSERT INTO `users` (`email`,`password`,`organization`,`role`,`active`) VALUES (:email,:pass,:org,:role,:actief)');
			try{
				$sth->execute(array(
					':email' => $email,
					':pass' => $password,
					':org' => $organization,
					':role' => $role,
					':actief' => $activateUrl
				));
				if($this->sendActivationMail($email,$this->getOrganizationById($organization),$activateUrl)){
					echo "verstuurd";
				}else{
					echo "nee";
				}
				return TRUE;
			} catch (PDOException $e) {
				print "Error!: " . $e->getMessage() . "<br/>";
			}
		}
	}

	public function update($uid,$email,$password,$role){
		if($password == false){
			$q = 'UPDATE `users` SET `Uemail`=:emailWHERE `Uid` = :id';
			$param = array(
					':email' => $email,
                    ':id' => $uid
				);
		}else{
			$q = 'UPDATE `users` SET `Uemail`=:email,`Upass`=:pass WHERE `Uid` = :id';
            $salt1 = $this->rand_str(8);
            $salt2 = $this->rand_str(8);
			$param = array(
					':email' => $email,
					':pass' => $this->pwhash($password, $salt1, $salt2),
                    ':id' => $uid
				);
			
		}
		
			$sth = $this->db->prepare($q);
			try{
				$sth->execute($param);
				return TRUE;
			} catch (PDOException $e) {
				print "Error!: " . $e->getMessage() . "<br/>";
			}
		
	}

	public function delete($id){
			$sth = $this->db->prepare("DELETE FROM users WHERE Uid=:id");
	        $sth->execute(array(
	        	':id' => $id
	        ));
	        return (count($sth->rowCount()) > 0)? TRUE: FALSE;
	}

	private function sendActivationMail($to,$organization,$activateUrl){
		$subject = 'Activering account Project.DualBrands.nl';
		
		$headers = "From: no-reply@dualbrands.nl \r\n";
		$headers .= "Reply-To: info@dualbrands.nl \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$mssg  = '<html><body style="font-family: Arial;">';
		$mssg .= '<img src="http://'.$_SERVER['SERVER_NAME'].'/public/img/logo.png" style="display:inline;vertical-align:top;" /><div style="display:inline-block;max-width:550px;">';
		$mssg .= '<h3>Er is een account voor uw aangemaakt op project.dualbrands.nl namens '.$organization.'</h3>';
		$mssg .= '<p>Met een account op onze website kunt u gebruik maken van leuke acties en kunt u gemakkelijk bestellingen voltooien via onze webshop.</p>';
		$mssg .= '<p>Om uw account te kunnen gebruiken moet u deze nog activeren dit kunt u doen door op de volgende link te klikken: <br />';
		$mssg .= '<a href="http://'.$_SERVER['SERVER_NAME'].'/user/activate/'.$activateUrl.'" style="word-break:break-all;">http://'.$_SERVER['SERVER_NAME'].'/login/activate/'.$activateUrl.'</a</p>';
		$mssg .= '</div></body></html>';
		return (mail($to, $subject, $mssg, $headers)) ? TRUE : FALSE;
	}

	private function email_exists($email){
			$sth = $this->db->prepare('SELECT `email` FROM users WHERE `email`=:email');
			try{
				$sth->execute(array(
					':email' => $email
				));
				if($sth->rowCount() == 0){
					return FALSE;
				}else{
					return TRUE;
				}
			} catch (PDOException $e) {
				print "Error!: " . $e->getMessage() . "<br/>";
			}
		
	}

	/*========================================================*\
	 *	$FORMATTING
	\*========================================================*/
}