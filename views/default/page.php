<!DOCTYPE html>
<html>
<head>
    <title>DualBrands MVC</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

	<?php
    if (isset($this->css)) {
        foreach($this->css as $css) {
            echo '<link rel="stylesheet" href="/public/css/'.$css.'" />';
        }
    }
    ?>
</head>
<body>

<?php if (isset($this->sidebar)) { $this->loadTpl( $this->sidebar ); } ?>

<!-- Content -->
<div id="content">
<?php if (isset($this->top))     { $this->loadTpl( $this->top ); } ?>
<?php if (isset($this->main))    { $this->loadTpl( $this->main ); } ?>
</div>
<!-- // Content END -->

<?php if (isset($this->footer))  { $this->loadTpl( $this->footer ); }  else { $this->loadTpl("footer.php" ); } ?>

<?php 
if (isset($this->js)){
    foreach($this->js as $js):
	 	if (!empty($js)) echo '<script src="/public/js/'.$js.'"></script>';
	endforeach; 
}; 
?>
</body>
</html>