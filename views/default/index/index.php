<a href='#' id="resend">Fire again!</a> <a href="/README.md">README</a>
<script type="text/javascript">
    $(document).ready(function() {
    /*
    		var data = {
    			'entities' : [{
    				'name':'news',
    				'fields': ['title','body']
    				},{
    				'name':'about'
    				}]
    		};
    		data = {};
    		var url = '/~wistour/index/all';
    	*/
    	/* User*/
	    	 //login 
	    	var data = {
		    	'name' : 'Wouter',
		    	'pass' : '1visvis1'
	    	};
	    	var url = '/user/login';
	    	
	    	/* getCurrent 
	    	var data = {};
	    	var url = '/~wistour/user/current';
	    	*/
	    	/* logOut 
	    	var data = {};
	    	var url = '/~wistour/user/logout';
	    	*/
    	/* end user*/		
	    /*------------------------*\
	        $Dynamische objecten
	    \*------------------------*/	
	    	/*
	    		De volgende objecten zijn dynamische objecten, deze hebben allemaal de zelfde controller. 
	    		Met de object naam in de url bepaal je de welk object er wordt gecalld.
	    		Een overzicht van de objecten en de mogelijke veld namen vind je hier onder:
	    			- about | url=/~wistour/about/{actie} | fields=array('id'=>'int','title'=>'str','description'=>'blob','url'=>'str');
	    			- enquete | url=/~wistour/enquete/{actie} | fields=array('id'=>'int','title'=>'str','question'=>'blob','type'=>'str');
	    			- news | url=/~wistour/news/{actie} | fields=array('id'=>'int','title'=>'str','body'=>'blob','createdOn'=>'timestamp');
	    			- practice | url=/~wistour/pratice/{actie} | fields=array('id'=>'int','title'=>'str','question'=>'blob','answer'=>'blob','difficulty'=>'int','createdOn'=>'timestamp');
	    			- program | url=/~wistour/program/{actie} |fields= array('id'=>'int','title'=>'str','description'=>'blob','body'=>'blob','createdOn'=>'timestamp');
	    			- sponsors | url=/~wistour/sponsors/{actie} | fields=array('id'=>'int','title'=>'str','description'=>'blob','url'=>'str');
	    			- study | url=/~wistour/study/{actie} | fields=array('id'=>'int','title'=>'str','content'=>'blob','date'=>'timestamp');
	    				    		
	    		De volgende comments zijn voorbeelden van de functies voor het object news.
	    		[HENCE] bij de *by functions kunnen op meerdere velden filteren, de *byId functions kunnen alleen op id filteren die in het zelfde object staat als de rest.
	    	*/
    		/* getAll 
    		// bij getAll en getBy zijn fields is niet verplicht
    		var data = {
    			'fields' : ['title','body']
    		}
    		var url = '/~wistour/news/getAll';
    		*/
    		/* getBy 
    		// values hoeft niet perse id te zijn maar mag ook een van de andere fields zijn
    		var data = { 
    			'values': {'id' : 1 },
    			'fields' : ['title','body']
    		}
    		var url = '/~wistour/news/getBy';
    		*/
    		/* add 
    		var data = {
    			'title' : 'test1',
    			'body' : '<h1>titel</h1><p>test</p>'
    		}
    		var url = '/~wistour/news/add';
    		*/
    		/* updateById
    		var data = {
    			'id' : 1,
    			'title' : 'test34',
    			'body' : '<h1>titel</h1><p>test</p>'
    		}
    		var url = '/~wistour/news/updateById';
    		*/
    		
    		/* deleteBy 
    		var data = {
    			'id' : 2
    		}
    		var url = '/~wistour/news/deleteBy';
			*/
    		
    	/* end dynamische objecten */
    	
    	
    	$('#resend').on('click',function(e){
	    	call(data,url);
    	});
    	function call(data,url){
    		$.ajax({
				'type' : 'POST',
				'async': 'true',
				'data' : data,
				'dataType' : 'jsonp',
				'url' : window.location.protocol + "//" + window.location.hostname + url,
				'success' : function(data,textStatus){
					console.log(data,textStatus);
				},
				'error' : function(jqXHR, textStatus, errorThrown){
					console.log(jqXHR, textStatus, errorThrown);
				}				
			});
    	}
    	call(data,url);
    });
</script>