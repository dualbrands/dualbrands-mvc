<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Dual Brands" />
		<meta charset="utf-8" />
		<title>Bakkerij van Ooijen</title>
		<!-- don't forget to paste your page title! -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="" />
		<meta name="author" content="" />
	    <link rel="shortcut icon" href="assets/ico/favicon.png">
	    <link href="/public/css/bootstrap.css" rel="stylesheet">
	    <link href="/public/css/theme.css" rel="stylesheet">
	    <link href="/public/css/font-awesome.min.css" rel="stylesheet">
	    <link href="/public/css/alertify.css" rel="stylesheet">
	    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
	    <link rel="Favicon Icon" href="favicon.ico">
	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->

		<?php
			if (isset($this->css)){
				foreach ($this->css as $css){
					echo '<link rel="stylesheet" href="/public/css/'.$css.'">';
				}
			}
		?>
	</head>
	<body class="">
	<div id="wrap">
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <div class="logo"> 
            <img src="/public/img/theme/logo.png" alt="Realm Admin Template">
          </div>
           <a class="btn btn-navbar visible-phone" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
           <a class="btn btn-navbar slide_menu_left visible-tablet">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          <div class="top-menu visible-desktop">
            <ul class="pull-left">
              <li><a id="messages" data-notification="2" href="#"><i class="icon-envelope"></i> Messages</a></li>
              <li><a id="notifications" data-notification="3" href="#"><i class="icon-globe"></i> Notifications</a></li>
            </ul>
            <ul class="pull-right">  
              <li><a href="/login/logout"><i class="icon-off"></i> Logout</a></li>
            </ul>
          </div>

          <div class="top-menu visible-phone visible-tablet">
            <ul class="pull-right">  
              <li><a title="link to View all Messages page, no popover in phone view or tablet" href="#"><i class="icon-envelope"></i></a></li>
              <li><a title="link to View all Notifications page, no popover in phone view or tablet" href="#"><i class="icon-globe"></i></a></li>
              <li><a href="/login/logout"><i class="icon-off"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php if($this->sidebar == TRUE): ?>
    	<div class="container-fluid">
    		<div class="sidebar-nav nav-collapse collapse">
			<?=$this->renderMenu();?>
    		</div>
    	</div>
    <?php endif; ?>
      <!-- Main window -->
      <div class="main_container" >