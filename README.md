DualBrands MVC
===============

This project is a lightweight MVC framework written in PHP handcrafted by DualBrands.
The DBMVC is designed to speed up the developing process of different types of applications.
Several practical implementations may range from a simple website, a RESTful API, to advanced custom web-applications.

Example config.yml
--------------
The framework uses an YML config file as shown below.

	databases:
		- type:   mysql
		  host:   localhost
		  name:   database
		  user:   root
		  pass:   pass
	keys:
		private:	YOURPRIVATEKEYCOMESHERE
		salt:	YOURSALTCOMESHERE
	allowedEntities:
		- example
		- example2